extends Node

# (Thing) is a typehint for the compiler
# res:// is the root folder of the game'
# `preload` loads the scene when the game is compiled
# `load` loads the scene on demand
export (PackedScene) var mob_scene = preload("res://Mob.tscn")


func _ready():
    randomize()


# event is an object of type InputEvent with just the key that was pressed
# convenient for one-time actions
func _unhandled_input(event):
    if event.is_action_pressed("ui_accept") and $UserInterface/Retry.visible:
        # represents entire tree of nodes in the game
        get_tree().reload_current_scene()


# triggers every time the MobTimer timeout signals (AKA event trigger)
func _on_MobTimer_timeout():
    var mob = mob_scene.instance()

    var mob_spawn_location = $SpawnPath/SpawnLocation
    # randf generates a random value between 0 and 1
    mob_spawn_location.unit_offset = randf()

    var player_position = $Player.transform.origin

    add_child(mob)
    mob.initialize(mob_spawn_location.translation, player_position)
    mob.connect("squashed", $UserInterface/ScoreLabel, "_on_Mob_squashed")


func _on_Player_hit():
    $MobTimer.stop()
    $UserInterface/Retry.show()
