extends KinematicBody

signal hit

# exported variables can be edited in the editor
export var speed = 14.0
export var jump_impulse = 30.0
export var fall_acceleration = 75
export var bounce_impulse = 16.0

var velocity = Vector3.ZERO

func _physics_process(delta):
    # called 60 times per second by default
    # similar to _process but for physics, such as RigitBody or KinematicBody
    var direction = Vector3.ZERO

    if Input.is_action_pressed("move_right"):
        direction.x += 1
    if Input.is_action_pressed("move_left"):
        direction.x -= 1

    if Input.is_action_pressed("move_back"):
        direction.z += 1
    if Input.is_action_pressed("move_forward"):
        direction.z -= 1

    if direction != Vector3.ZERO:
        # normalized() ensures direction always has a length of 1
        direction = direction.normalized()
        # $ gets node of the same name in scene
        # `translation` is the 3D equivalent of 2D `position`; location relative to its parent
        # 2nd param is what is the UP direction
        # this line will make the character turn to look in the direction it's moving (!)
        $Pivot.look_at(translation + direction, Vector3.UP)

    if is_on_floor() and Input.is_action_pressed("jump"):
        velocity.y += jump_impulse

    velocity.y -= fall_acceleration * delta

    # want to treat y axis differently from x and z so we calculate these separately
    velocity.x = direction.x * speed
    velocity.z = direction.z * speed

    # returns new velocity with loss in momentum
    velocity = move_and_slide(velocity, Vector3.UP)

    # the number of times your node collided when calling move_and_slide in this frame
    for index in get_slide_count():
        var collision = get_slide_collision(index)
        if collision.collider.is_in_group("mob"):
            var mob = collision.collider
            # dot() is dot-product
            # collision.normal is a vector perpendicular to the collision
            # dot product will be a value close to 1 when they align
            # and a value close to 0 when they are parallel
            if Vector3.UP.dot(collision.normal) > 0.1:
                mob.squash()
                velocity.y = bounce_impulse

func die():
    emit_signal("hit")
    queue_free()

func _on_MobDetector_body_entered(body):
    die()
